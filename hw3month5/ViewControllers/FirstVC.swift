//
//  ViewController.swift
//  hw3month5
//
//  Created by Александр Калашников on 20/9/22.
//

import UIKit
import SnapKit

class FirstVC: UIViewController {

    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        layout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func layout() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "bag"), style: .plain, target: self, action: #selector(goBasket))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(goAdd))
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.right.left.equalToSuperview().inset(12)
            make.top.equalToSuperview().inset(80)
            make.bottom.equalToSuperview().inset(10)
        }
    }
    
    @objc func goAdd() {
        let vc = SecondVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func goBasket() {
        let vc = ThirdVC()
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension FirstVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductModel.shared.prodcutsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = ProductModel.shared.prodcutsArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ProductModel.shared.basketProductsArr.append(ProductModel.shared.prodcutsArr[indexPath.row])
    }
}
