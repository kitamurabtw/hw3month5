import UIKit
class SecondVC: UIViewController {
    private lazy var nameProduct: UITextField = {
        let view = UITextField()
        return view
    }()
    
    private lazy var priceProduct: UITextField = {
        let view = UITextField()
        return view
    }()
    
    private lazy var okButton: UIButton = {
        let view = UIButton()
        view.addTarget(self, action: #selector(okAct), for: .touchUpInside)
        return view
    }()
    
    lazy var viewModel = {
        return SecondViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout()
    }
    
    func layout() {
        view.backgroundColor = .white
        
        view.addSubview(nameProduct)
        nameProduct.backgroundColor = .lightGray
        nameProduct.layer.cornerRadius = 10
        nameProduct.layer.borderWidth = 1
        nameProduct.placeholder = "   Название продукта"
        nameProduct.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(300)
            make.centerX.equalToSuperview().inset(0)
            make.height.equalTo(45)
            make.width.equalTo(250)
        }
        view.addSubview(priceProduct)
        priceProduct.backgroundColor = .lightGray
        priceProduct.layer.cornerRadius = 10
        priceProduct.layer.borderWidth = 1
        priceProduct.placeholder = "   Цена продукта"
        priceProduct.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(350)
            make.centerX.equalToSuperview().inset(0)
            make.height.equalTo(45)
            make.width.equalTo(250)
        }
        view.addSubview(okButton)
        okButton.backgroundColor = .green
        okButton.layer.cornerRadius = 10
        okButton.layer.borderWidth = 1
        okButton.setTitle("Добавить", for: .normal)
        okButton.tintColor = .black
        okButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(400)
            make.centerX.equalToSuperview().inset(0)
            make.height.equalTo(40)
            make.width.equalTo(100)
        }
    }
    
    @objc func okAct() {
        viewModel.text = "Имя продукта: \(String(describing: nameProduct.text!)), цена: \(priceProduct.text!)"
    }
}
