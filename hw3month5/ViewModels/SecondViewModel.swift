//
//  SecondViewModel.swift
//  hw3month5
//
//  Created by Александр Калашников on 21/9/22.
//

import Foundation

class SecondViewModel: NSObject {
    @objc dynamic var text = ""
    var textObservation: NSKeyValueObservation?
    lazy var secondModel = {
        return SecondModel()
    }()
    var onUpdateName: ((String) -> Void)?
    
    override init() {
        super.init()
        textObservation = observe(\.text, options: .new, changeHandler: {[weak self] vm, change in
            guard let ggName = change.newValue else {return}
            self?.secondModel.name = ggName
        })
        secondModel.onGetNewName = {[weak self] newName in
            self?.onUpdateName?(newName)
        }
    }
}
