//
//  SecondModel.swift
//  hw3month5
//
//  Created by Александр Калашников on 21/9/22.
//

import Foundation

class SecondModel: NSObject {
    @objc dynamic var name = ""
    var nameObservation: NSKeyValueObservation?
    var onGetNewName: ((String) -> Void)?
    
    override init() {
        super.init()
        nameObservation = observe(\.name, options: .new, changeHandler: {[weak self] model, change in
            guard let name = change.newValue else {return}
            self?.onGetNewName?(name)
            ProductModel.shared.text = name
        })
        
        if ProductModel.shared.text != ""{
            ProductModel.shared.prodcutsArr.append(ProductModel.shared.text)
            print(ProductModel.shared.prodcutsArr)
        }
    }
}
